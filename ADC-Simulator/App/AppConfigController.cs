﻿using System;
using System.IO;
using System.Xml.Serialization;
using MonoMac.Foundation;
using System.Net;

namespace ADCSimulator
{
	public class AppConfigController
	{
		public AppConfigController ()
		{
			if (m_AppConfig == null) {
				m_AppConfig = LoadAppConfig ();
			}
		}

		private static AppConfigController m_Instance;

		public AppConfig m_AppConfig;

		public static AppConfigController Instance 
		{
			get {
				if (m_Instance == null) { 
					m_Instance = new AppConfigController ();
				}
				return m_Instance;
			}
		}

		private AppConfig LoadAppConfig() {
			if (!File.Exists (Path.Combine (NSBundle.MainBundle.BundlePath, "AppConfig.xml"))) {
				// the config file does not exist - so lets make a hardcoded entry here
				Console.WriteLine ("No AppConfig.xml - create default directory");
				AppConfig defaultAppConfigObject = new AppConfig ();
				defaultAppConfigObject.ConfigFileDirectory = "Users/josephnarai/Projects/ADC-Simulator/ConfigFiles";
				defaultAppConfigObject.UnitTestDirectory = "Users/josephnarai/Projects/ADC-Simulator/ConfigFiles";
				defaultAppConfigObject.ServerAddress = "10.0.0.123";
				defaultAppConfigObject.ServerPort = "1338";
				return defaultAppConfigObject;
			} else {
				try {
					using (TextReader reader = new StreamReader (Path.Combine (NSBundle.MainBundle.BundlePath, "AppConfig.xml"))) {
						XmlSerializer serializer = new XmlSerializer (typeof(AppConfig));
						AppConfig xmlAppConfigOjbect = (AppConfig)serializer.Deserialize (reader);
						return xmlAppConfigOjbect;
					}
				} catch (Exception e) {
					Console.WriteLine ("LoadAppConfig - Exception Caught:" + e.Message);
					return new AppConfig();
				}
			}
		}

		public void SaveAppConfig() {

			using (TextWriter writer = new StreamWriter(Path.Combine(NSBundle.MainBundle.BundlePath,"AppConfig.xml"))) {
				XmlSerializer serializer = new XmlSerializer(typeof(AppConfig));
				serializer.Serialize(writer,m_AppConfig);
			}
		}

	}
}

