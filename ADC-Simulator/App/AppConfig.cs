﻿using System;
using System.Net;


namespace ADCSimulator
{
	public class AppConfig
	{
		public string ConfigFileDirectory	{ get; set; }
		public string UnitTestDirectory		{ get; set; }
		public string ServerAddress			{ get; set; }
		public string ServerPort			{ get; set; }
		public bool RepeatUnitTests			{ get; set; }

		public AppConfig ()
		{
		}
	}
}

