﻿using System;

namespace ADCSimulator
{
	public class ADCSystemFault
	{
		public bool		RouterLost			{ get; set; }	// Lost comms >= 1 routers
		public bool		BothLampsFailed		{ get; set; }	// Both lamps failed on >= 1 routers
		public bool		SingleLampFailure	{ get; set; }	// A single lamp failed on >= 1 routers
		public bool 	SensorFailure		{ get; set; }	// Sensor failure on >= 1 routers
		public bool		BatteryFailure		{ get; set; }	// Battery low on >= 1 routers
		public bool		SolarFailure		{ get; set; }	// Solar failed on >= 1 routers
		public bool		TurbineFailure		{ get; set; }	// Turbine voltage low on >= 1 routers
		public bool		TestModeEnabled		{ get; set; }	// Test mode on >= 1 routers
		public bool		IsDark				{ get; set; }	// Lamps off (dark/disabled mode) on >= 1 routers
		public bool		DoorOpen			{ get; set; }	// Door open on >= 1 routers

		public ADCSystemFault ()
		{
		}
	}
}

