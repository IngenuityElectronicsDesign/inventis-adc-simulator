﻿using System;

namespace ADCSimulator
{
	public class ADCHeader
	{
		public int					Created					{ get; set; }	// DateTime this data was created realtive to UTC as a ulong in seconds since 1970
		public int					LocalTimeOffset			{ get; set; }	// Local Time offset for this location
		public string				Project					{ get; set; }	// The name of the project that this device belongs to
		public string				Site					{ get; set; }	// The name of the site this device is located at ( This is combined with Dest1 and Dest2 to make a unique ID for this device )
		public string				Dest1					{ get; set; }	// The name of the first destination  this device signals to
		public string				Dest2					{ get; set; }	// The name of the second destination this device signals to
		public double				AvgSpeed				{ get; set; }	// The average speed of something at this location
		public int					Channel					{ get; set; }	// The channel this device belongs to
		public int					Network					{ get; set; }	// The network this device belongs to
		public int					SoftwareVersion			{ get; set; }	// The software version of the software installed on this device
		public int					VersionFlag				{ get; set; }	// The version flag for the version of software installed on this device
		public double				Longitude				{ get; set; }	// The longitude of the location of this device
		public double				Latitude				{ get; set; }	// The latitude of this location of this device
		public int					Tracks					{ get; set; }	// The number of tracks at this location
		public double[]				TrackDistance			{ get; set; }	// The distance of each of these tracks from this device
		public int					AttachedRouters			{ get; set; }	// The total number of routers in this site (including this ADC which is always router 0)
		public ADCRouterData[]		AttachedRouterData		{ get; set; }	// The router data for each attached router
		public int					OnlineRouters			{ get; set; }	// The number of routers currently online
		public ADCSystemFault		SystemFault				{ get; set; }	// The system fault state for this ADC
		public ADCRouterLog[]		RouterLogs				{ get; set; }	// The router logs for this system
		public ADCEventLog[]		EventLogs				{ get; set; }	// The event logs for this system

		public ADCHeader ()
		{
		}
	}
}

