﻿using System;

namespace ADCSimulator
{
	public class ADCRouterFault
	{
		public bool		Lamp1Fault			{ get; set; }	// Lamp1 fault
		public bool 	Lamp2Fault			{ get; set; }	// Lamp2 fault
		public bool		RadarTriggerError	{ get; set; }	// Radar trigger error
		public bool		SolarCritical		{ get; set; }	// Solar voltage low for > 24hrs
		public bool		BatteryCritical		{ get; set; }	// Battery below critical
		public bool		DoorOpen			{ get; set; }	// Door open
		public bool		TestModeEnabled		{ get; set; }	// Router is in Test Mode

		public ADCRouterFault ()
		{
		}
	}
}

