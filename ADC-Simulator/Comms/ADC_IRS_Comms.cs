﻿using System;
using Newtonsoft.Json;

namespace ADCSimulator
{
	public class ADC_IRS_Comms
	{
		#region JSON Parameters
		public int			ProtocolVersion 	{ get; set; }	// The protocol version for this ADC_IRS_Comms message
		public string		DeviceIdentifier	{ get; set; }	// The Device Identifier of the ADC we are communicating with is generated from Site_Dest1_Dest2 concatinated strings
		public int			Created				{ get; set; }	// DateTime this data was created realtive to UTC as a ulong in seconds since 1970
		public string		Response			{ get; set; }	// Command as string ( see CommsCommands in AsynchronousClient.cs )
		public ADCHeader	ADCData				{ get; set; }	// Option ADCData contained in this message ( depending on the command )
		#endregion

		// Private class parameters
		private readonly int		c_protocolVersion	= 1;

		public ADC_IRS_Comms (string response, string deviceIdentifier, ADCHeader adcData=null )
		{
			ProtocolVersion		= c_protocolVersion;
			DeviceIdentifier	= deviceIdentifier;
			Created 			= ConvertToUnixTimestamp (DateTime.Now);
			Response 			= response;
			ADCData 			= adcData;
		}

		public static DateTime ConvertFromUnixTimestamp(int timestamp)
		{
			DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
			return origin.AddSeconds(timestamp);
		}

		public static int ConvertToUnixTimestamp(DateTime date)
		{
			DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
			TimeSpan diff = date.ToUniversalTime() - origin;
			return (int)Math.Floor(diff.TotalSeconds);
		}

		public static ADC_IRS_Comms Deseralise( string incomingString ) {

			ADC_IRS_Comms returnObject;

			try {
				returnObject = JsonConvert.DeserializeObject<ADC_IRS_Comms>(incomingString);
			} catch (Exception e) {
				Console.WriteLine ("Excpetion when deserlializing ADC_IRS_Comms data : " + e.Message + " (" + incomingString + ")" );
				returnObject = null;
			}
			return returnObject;
		}

		public string Seralise() {

			string returnString;

			try {
				returnString = JsonConvert.SerializeObject(this);
			} catch (Exception e) {
				Console.WriteLine ("Excpetion when Serlializing to ADC_IRS_Comms data : " + e.Message );
				returnString = null;
			}
			//Console.WriteLine ("---- JSON Deserliase:\n" + JsonConvert.SerializeObject (this, Formatting.Indented));
			return returnString;
		}

	}
}

