﻿using System;

namespace ADCSimulator
{
	public class ADCEventLog
	{
		public int						Created			{ get; set; }	// DateTime this data was created realtive to UTC as a ulong in seconds since 1970
		public string					Event			{ get; set; }	// Event String from ADC device
	}
}

