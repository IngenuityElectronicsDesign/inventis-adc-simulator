﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Timers;

namespace ADCSimulator
{
	// State object for receiving data from remote device.
	public class StateObject {
		// Client socket.
		public Socket 				workSocket 		= null;
		// Size of receive buffer.
		public const int 			BufferSize 		= 4096;
		// Receive buffer.
		public byte[] 				buffer 			= new byte[BufferSize];
		// Received data string.
		public StringBuilder 		sb 				= new StringBuilder();
	}

	public class AsynchronousClient {

		// AsyncStateMachineEnum for passing to the callback to let outsiders know what state the comms are in
		public enum StateMachineEnum {
			InitiateConnection,
			WaitingForConnectCallback,
			WaitingForCommand,
			WaitingForSentCallback,
			WaitingForEndSessionSentCallback,
			ConnectionClosed,

			Count
		}

		public enum CommsCommand {
			SetDateTime,
			RequestStatus,
			EnableRouter,
			DisableRouter,
			EndSession,

			Count
		}

		public enum CommsResponse {
			StatusResponse,
			ACK,
			NAK,

			Count
		}

		private enum NextStateCalledFrom {
			Start,
			ConnectCallback,
			SendCallback,
			ReceiveCallback,

			Count
		}

		private int									m_CommsId					= 0;
		private string								m_deviceIdentifier			= null;
		private ADCHeader 							m_adcData 					= null;
		private int 								m_hostPort 					= 0;
		private IPAddress							m_hostIpAddress 			= null;
		private Socket 								m_client 					= null;
		private Action<string> 						m_ActivityDebugCallback		= null;
		private const int							c_TimeoutTime				= 10000;
		private Timer 								m_TimeoutTimer 				= null;

		// The state of our connection and the callback to the outside world
		private StateMachineEnum					m_commsState 				= StateMachineEnum.ConnectionClosed;
		private Action<StateMachineEnum,Exception>	m_callback					= null;

		public AsynchronousClient() {
		}

		private void Debug(string debugString) {
			string debugOutputString = "[" + m_CommsId + "] " + debugString;
			Console.WriteLine (debugOutputString);
			if (m_ActivityDebugCallback != null) {
				m_ActivityDebugCallback (debugOutputString);
			}
		}

		private void OnTimeoutTimerElasped (object o, EventArgs e)
		{
			Debug ("--- Error State Timeout:" + m_commsState);
			m_TimeoutTimer.Stop ();
			SetCommsState (StateMachineEnum.ConnectionClosed, new Exception ("Timeout: " + m_commsState + " (" + (int)m_commsState + ")"));
		}

		private void SetCommsState( StateMachineEnum newState, Exception e=null ) {
			if (m_commsState != newState) {
				Debug ("--- " + m_commsState.ToString () + " -> " + newState.ToString ());
				// set the state
				m_commsState = newState;
				// tell the outside world our state
				m_callback (m_commsState, e);
			}
		}

		private bool SetNextCommsState( NextStateCalledFrom nextStateCalledFrom, string response = null, string responseParameter = null) {

			if (m_client == null && !( m_commsState == StateMachineEnum.ConnectionClosed || m_commsState == StateMachineEnum.InitiateConnection ) ) {
				SetCommsState (StateMachineEnum.ConnectionClosed);
				return false;
			}

			switch (m_commsState) {
				case StateMachineEnum.InitiateConnection:
					// set the next state
					SetCommsState (StateMachineEnum.WaitingForConnectCallback);
					// Connect to a remote device.
					try {
						IPEndPoint remoteEP = new IPEndPoint(m_hostIpAddress, m_hostPort);
						// Create a TCP/IP socket.
						m_client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
						// Connect to the remote endpoint.
						m_client.BeginConnect( remoteEP, new AsyncCallback(ConnectCallback), m_client);
					} catch (Exception e) {
						SetCommsState (StateMachineEnum.ConnectionClosed, e);
						return false;
					}
					break;
				case StateMachineEnum.WaitingForConnectCallback:
					if (nextStateCalledFrom == NextStateCalledFrom.ConnectCallback) {
						SetCommsState (StateMachineEnum.WaitingForCommand);
						// Set up to wait for the response
						Receive ();
					} else {
						SetCommsState(StateMachineEnum.ConnectionClosed,new Exception("SetNextCommsState - StartReadySent - next state not called from SendCallback") );
					}
					break;
				case StateMachineEnum.WaitingForCommand:
					if (nextStateCalledFrom == NextStateCalledFrom.ReceiveCallback) {
						if (response != null) {
							if (response.Equals (CommsCommand.SetDateTime.ToString(), StringComparison.Ordinal)) {
								Debug ("### SetDateTime to " + responseParameter);
								SetCommsState (StateMachineEnum.WaitingForSentCallback);
								// Send the status response message
								Send ( new ADC_IRS_Comms(CommsResponse.ACK.ToString(), m_deviceIdentifier ) );
							} else if (response.Equals (CommsCommand.RequestStatus.ToString(), StringComparison.Ordinal)) {
								SetCommsState (StateMachineEnum.WaitingForSentCallback);
								// Send the status response message
								Send ( new ADC_IRS_Comms(CommsResponse.StatusResponse.ToString(), m_deviceIdentifier, m_adcData) );
							} else if (response.Equals (CommsCommand.EnableRouter.ToString(), StringComparison.Ordinal)) {
								Debug ("### Enable router:" + responseParameter);
								SetCommsState (StateMachineEnum.WaitingForSentCallback);
								// Send the status response message
								Send ( new ADC_IRS_Comms(CommsResponse.ACK.ToString(), m_deviceIdentifier ) );
							} else if (response.Equals (CommsCommand.DisableRouter.ToString(), StringComparison.Ordinal)) {
								Debug ("### Disable router:" + responseParameter);
								SetCommsState (StateMachineEnum.WaitingForSentCallback);
								// Send the status response message
								Send ( new ADC_IRS_Comms(CommsResponse.ACK.ToString(), m_deviceIdentifier ) );
							} else if (response.Equals (CommsCommand.EndSession.ToString(), StringComparison.Ordinal)) {
								SetCommsState (StateMachineEnum.WaitingForEndSessionSentCallback);
								// Send the status response message
								Send ( new ADC_IRS_Comms(CommsResponse.ACK.ToString(), m_deviceIdentifier ) );
							} else {
								SetCommsState (StateMachineEnum.ConnectionClosed, new Exception ("SetNextCommsState - WaitingForCommand - Unrecognised Command:" + response));
							}
						} else {
							SetCommsState (StateMachineEnum.ConnectionClosed, new Exception ("SetNextCommsState - WaitingForCommand - null response"));
						}
					} else {
						SetCommsState(StateMachineEnum.ConnectionClosed,new Exception("SetNextCommsState - WaitingForCommand - next state not called from SendCallback") );
					}
					break;
				case StateMachineEnum.WaitingForSentCallback:
					if (nextStateCalledFrom == NextStateCalledFrom.SendCallback) {
						SetCommsState (StateMachineEnum.WaitingForCommand);
						// Set up to wait for the response
						Receive ();
					} else {
						SetCommsState(StateMachineEnum.ConnectionClosed,new Exception("SetNextCommsState - WaitingForSetDateTimeAckSentCallback - next state not called from SendCallback") );
					}
					break;
				case StateMachineEnum.WaitingForEndSessionSentCallback:
					if (nextStateCalledFrom == NextStateCalledFrom.SendCallback) {
						// terminate the connection - THIS WILL SET THE STATE
						StopClient ();
					} else {
						SetCommsState(StateMachineEnum.ConnectionClosed,new Exception("SetNextCommsState - WaitingForEndSessionSentCallback - next state not called from SendCallback") );
					}
					break;
				case StateMachineEnum.ConnectionClosed:
					// We stay here - the start client will set it back to idle and kick things off again
					if (m_TimeoutTimer != null) {
						m_TimeoutTimer.Stop ();
					}
					return true;
				default:
					SetCommsState(StateMachineEnum.ConnectionClosed,new Exception("SetNextCommsState - Unkown m_commsState:" + m_commsState + " (" + (int)m_commsState + ")") );
					return false;
			}

			// Start the timeout timer
			if (m_TimeoutTimer != null) {
				m_TimeoutTimer.Stop ();
				m_TimeoutTimer.Start ();
			}
			return true;
		}

		public bool StartClient( Action<StateMachineEnum,Exception>callback, int commsId, ADCHeader adcData, string hostUpAddress, string hostPort, Action<string> activityDebugCallback ) {

			if (m_TimeoutTimer == null) {
				m_TimeoutTimer = new Timer ();
				m_TimeoutTimer.Elapsed += OnTimeoutTimerElasped;
				m_TimeoutTimer.Interval = c_TimeoutTime;
				m_TimeoutTimer.AutoReset = false;
				m_TimeoutTimer.Stop ();
			}

			m_CommsId = commsId;
			m_deviceIdentifier = adcData.Site + "_" + adcData.Dest1 + "_" + adcData.Dest2;
			m_adcData = adcData;
			m_ActivityDebugCallback = activityDebugCallback;

			// remember our callback to the outside world
			m_callback	= callback;

			try {
				m_hostPort 		= Convert.ToInt32(hostPort);
			}
			catch ( Exception e ) {
				if ( e != null ) {
					SetCommsState(StateMachineEnum.ConnectionClosed,e);
					return false;
				}
			}

			try {
				m_hostIpAddress = IPAddress.Parse(hostUpAddress);
			} 
			catch ( Exception e ) {
				if ( e != null ) {
					SetCommsState(StateMachineEnum.ConnectionClosed,e);
					return false;
				}
			}

			// Establish the remote endpoint for the socket.
			// The name of the 
			// remote device is "host.contoso.com".
			//IPHostEntry ipHostInfo = Dns.Resolve("host.contoso.com");
			//IPAddress ipAddress = ipHostInfo.AddressList[0];
			if ( m_hostIpAddress == null ) {
				SetCommsState(StateMachineEnum.ConnectionClosed,new Exception("Start Client - unable to start - ServerAddress null") );
				return false;
			}
			if ( m_hostPort == 0 ) {
				SetCommsState(StateMachineEnum.ConnectionClosed,new Exception("Start Client - unable to start - ServerPort = 0") );
				return false;
			}

			// set us to idle before we start any new sessions
			SetCommsState(StateMachineEnum.InitiateConnection);

			// Kick off the state machine
			return SetNextCommsState(NextStateCalledFrom.Start);
		}

		public void StopClient() {
			// The exception to the state machine operation is terminate, which gets executed here, 
			// as there is no natural way for the state machine to terminate vie the SetNextCommsState mechanism
			SetCommsState(StateMachineEnum.ConnectionClosed);
			if (m_TimeoutTimer != null) {
				m_TimeoutTimer.Stop ();
				m_TimeoutTimer.Elapsed -= OnTimeoutTimerElasped;
				m_TimeoutTimer = null;
			}

			if (m_client != null) {
				try {
					if (m_client.Connected) {
						m_client.Shutdown (SocketShutdown.Both);
					}
					m_client.Close ();
				}
				catch (Exception te) {
					Debug("x-- StateMachine Terminate Exception:" + te.Message);
				}
				finally {
					m_client = null;
				}
			}
		}

		private void ConnectCallback(IAsyncResult ar) {
			try {
				// Retrieve the socket from the state object.
				Socket client = (Socket) ar.AsyncState;

				// Complete the connection.
				client.EndConnect(ar);

				Debug("    Socket connected to " + client.RemoteEndPoint.ToString() );

				// Keep the state machine going!
				SetNextCommsState(NextStateCalledFrom.ConnectCallback);
			} catch (Exception e) {
				Debug("ConnectCallback Exception:" + e.Message);
				// Somethings gone bad - reset the state machine
				StopClient ();
				SetCommsState (StateMachineEnum.ConnectionClosed, e);
			}
		}

		private void Receive() {
			if (m_client == null) {
				// Somethings gone bad - reset the state machine
				SetCommsState (StateMachineEnum.ConnectionClosed, new Exception("Receive - null socket"));
				return;
			}
			try {
				// Create the state object.
				StateObject state = new StateObject();
				state.workSocket = m_client;

				// Begin receiving the data from the remote device.
				m_client.BeginReceive( state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReceiveCallback), state);
			}
			catch (Exception e) {
				Debug("    " + e.ToString());
				// Somethings gone bad - reset the state machine
				SetCommsState (StateMachineEnum.ConnectionClosed, e);
			}
		}

		private void ReceiveCallback( IAsyncResult ar ) {
			try {
				// Retrieve the state object and the client socket 
				// from the asynchronous state object.
				StateObject state = (StateObject) ar.AsyncState;
				Socket client = state.workSocket;

				if ( !client.Connected ) {
					// this can happen after we close our connection if we are waiting for a command
					// this is bad design on Microsofts part - but lets just return us to an idle state without and exception as this is normal behaviour
					if ( m_commsState != StateMachineEnum.ConnectionClosed ) { 
						SetCommsState (StateMachineEnum.ConnectionClosed);
					}
					return;
				}

				// Read data from the remote device.
				int bytesRead = client.EndReceive(ar);

				//Debug("ReceiveCallback - bytesRead:" + bytesRead );

				if (bytesRead == StateObject.BufferSize) {

					Debug("ReceiveCallback - receive more data");

					// The object buffer is full - add the receveied data to our string bulider and then request more data!
					state.sb.Append(Encoding.ASCII.GetString(state.buffer,0,bytesRead));

					// Get the rest of the data.
					client.BeginReceive(state.buffer,0,StateObject.BufferSize,0, new AsyncCallback(ReceiveCallback), state);

				} else {

					if ( bytesRead > 0 ) {
						// append this buffers data to the string builder
						state.sb.Append(Encoding.ASCII.GetString(state.buffer,0,bytesRead));
					}

					// All the data has arrived; put it in response.
					if (state.sb.Length > 1) {
						string recString = state.sb.ToString();
						recString = recString.TrimEnd('\r','\n');
						string[] splitString = recString.Split(',');
						switch ( splitString.Length ) {
							case 0:
								SetCommsState (StateMachineEnum.ConnectionClosed, new Exception("Send - null socket") );
								break;
							case 1:
								Debug("    ReceiveCallback: " + recString + " Command:" + splitString[0] );
								SetNextCommsState(NextStateCalledFrom.ReceiveCallback,splitString[0]);
								break;
							case 2:
								Debug("    ReceiveCallback: " + recString + " Command:" + splitString[0] + " Parameter:" + splitString[1] );
								SetNextCommsState(NextStateCalledFrom.ReceiveCallback,splitString[0],splitString[1]);
								break;
							default:
								SetCommsState (StateMachineEnum.ConnectionClosed, new Exception("Send - null socket") );
								break;
						}
					}
				}
			} 
			catch (Exception e) {
				Debug("    " + e.ToString());
				// Somethings gone bad - reset the state machine
				SetCommsState (StateMachineEnum.ConnectionClosed, e);
			}
		}

		private void Send(ADC_IRS_Comms sendObj) {
			if (m_client == null) {
				// Somethings gone bad - reset the state machine
				SetCommsState (StateMachineEnum.ConnectionClosed, new Exception("Send - null socket") );
				return;
			}
			// Convert the string data to byte data using ASCII encoding.
			byte[] byteData = Encoding.ASCII.GetBytes(sendObj.Seralise());

			Debug("    Send Obj: " + sendObj.Response );

			// Begin sending the data to the remote device.
			m_client.BeginSend(byteData, 0, byteData.Length, 0, new AsyncCallback(SendCallback), m_client);
		}

		private void SendCallback(IAsyncResult ar) {
			try {
				// Retrieve the socket from the state object.
				Socket client = (Socket) ar.AsyncState;

				if ( !client.Connected ) {
					// this can happen after we close our connection if we are waiting for a send to complete
					// this is bad design on Microsofts part - but lets just return us to an idle state without and exception as this is normal behaviour
					SetCommsState (StateMachineEnum.ConnectionClosed);
					return;
				}

				// Complete sending the data to the remote device.
				client.EndSend(ar);
				//int bytesSent = client.EndSend(ar);
				//Debug("Sent {0} bytes to server.", bytesSent);

				// Keep the state machine going!
				SetNextCommsState(NextStateCalledFrom.SendCallback);

			} catch (Exception e) {
				Debug("    " + e.ToString());
				// Somethings gone bad - reset the state machine
				SetCommsState (StateMachineEnum.ConnectionClosed, e);
			}
		}

	}
}