﻿using System;

namespace ADCSimulator
{
	public class ADCRouterLog
	{
		public string					Id				{ get; set; }	// The Id of this router
		public int						Created			{ get; set; }	// DateTime this data was created realtive to UTC as a ulong in seconds since 1970
		public bool						IsSign			{ get; set; }	// Is there a sign attached to this router?
		public double[]					LampCurrent		{ get; set; }	// The lamp current for the lamps attached to this router
		public bool						LampOn			{ get; set; }	// Is the lamp on?
		public int						UpTime			{ get; set; }	// The time in seconds this router has been up
		public double 					BatteryVoltage	{ get; set; }	// The battery voltage for this router
		public double					SolarVoltage	{ get; set; }	// The solar voltage for this router
		public double					TurbineVoltage	{ get; set; }	// The turbine voltage for this router
		public double					AmbientLight	{ get; set; }	// The ambient light detected by this router location
		public double					Temperature		{ get; set; }	// The temperature of this router
		public int						Track			{ get; set; }	// The Track closest to this router
		public ADCRouterStatus			Status			{ get; set; }	// The Status of this router
		public ADCRouterFault			Fault			{ get; set; }	// Any Faults for this router

		public ADCRouterLog ()
		{
			Status = new ADCRouterStatus ();
			Fault = new ADCRouterFault ();
		}
	}
}

