﻿using System;

namespace ADCSimulator
{
	public class ADCRouterStatus
	{
		public bool		Active			{ get; set; }			// Router is activated
		public bool		DoorOpen		{ get; set; }			// Door is open
		public bool		SdCardError		{ get; set; }			// SD card error
		public bool		InvalidInput	{ get; set; }			// Invalid input detected
		public bool		BatteryLow		{ get; set; }			// Battery voltage too low
		public bool		SolarLow		{ get; set; }			// Solar voltage too low
		public bool		TurbineLow		{ get; set; }			// Turbine voltage too low
		public bool		LampNotFlashing	{ get; set; }			// lamp not flashing
		public bool		RouterLost		{ get; set; }			// router lost connection

		public ADCRouterStatus ()
		{
		}
	}
}

