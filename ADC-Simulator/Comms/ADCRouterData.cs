﻿using System;

namespace ADCSimulator
{
	public class ADCRouterData
	{
		public int		Id 			{ get; set; }	// Id of this router
		public bool		Online 		{ get; set; }	// Is this router currently online?
		public bool		IsSign		{ get; set; }	// Is this router a sign?
		public double	Distance	{ get; set; }	// Distance of this router from the ADC master

		public ADCRouterData ()
		{
		}
	}
}

