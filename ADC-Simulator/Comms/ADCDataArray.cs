﻿using System;
using System.Collections.Generic;

namespace ADCSimulator
{
	public class ADCDataArray
	{
		public ADCHeader[]	ADCDataArrayObject	{ get; set; }

		public int			UnitTestDelay		{ get; set; }
		public bool			SetCreatedToNow		{ get; set; }

		public ADCDataArray ()
		{
		}
	}
}

