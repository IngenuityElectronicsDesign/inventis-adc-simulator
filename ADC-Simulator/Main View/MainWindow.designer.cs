// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoMac.Foundation;
using System.CodeDom.Compiler;

namespace ADCSimulator
{
	[Register ("MainWindowController")]
	partial class MainWindowController
	{
		[Outlet]
		MonoMac.AppKit.NSTextView ActivityTextViewReference { get; set; }

		[Outlet]
		MonoMac.AppKit.NSTabView ADCTabViewReference { get; set; }

		[Outlet]
		MonoMac.AppKit.NSBrowser ConfigFileBrowser { get; set; }

		[Outlet]
		MonoMac.AppKit.NSTextField ConfigFileDirectoryLabelTextField { get; set; }

		[Outlet]
		MonoMac.AppKit.NSTextField ConfigFileDirectoryTextField { get; set; }

		[Outlet]
		MonoMac.AppKit.NSTextField ConfigParametersTextFieldReference { get; set; }

		[Outlet]
		MonoMac.AppKit.NSButton ExecuteUnitTestsButton { get; set; }

		[Outlet]
		MonoMac.AppKit.NSButton LoadConfigFileButton { get; set; }

		[Outlet]
		MonoMac.AppKit.NSTextField LoadedConfigFileReference { get; set; }

		[Outlet]
		MonoMac.AppKit.NSButton RepeatUnitTestsCheckboxReference { get; set; }

		[Outlet]
		MonoMac.AppKit.NSTextField ServerAddressTextField { get; set; }

		[Outlet]
		MonoMac.AppKit.NSTextField ServerPortTextField { get; set; }

		[Outlet]
		MonoMac.AppKit.NSTextField UnitTestDirectoryLabelTextField { get; set; }

		[Outlet]
		MonoMac.AppKit.NSTextField UnitTestDirectoryTextField { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (ActivityTextViewReference != null) {
				ActivityTextViewReference.Dispose ();
				ActivityTextViewReference = null;
			}

			if (ADCTabViewReference != null) {
				ADCTabViewReference.Dispose ();
				ADCTabViewReference = null;
			}

			if (ConfigFileBrowser != null) {
				ConfigFileBrowser.Dispose ();
				ConfigFileBrowser = null;
			}

			if (ConfigFileDirectoryLabelTextField != null) {
				ConfigFileDirectoryLabelTextField.Dispose ();
				ConfigFileDirectoryLabelTextField = null;
			}

			if (ConfigFileDirectoryTextField != null) {
				ConfigFileDirectoryTextField.Dispose ();
				ConfigFileDirectoryTextField = null;
			}

			if (ExecuteUnitTestsButton != null) {
				ExecuteUnitTestsButton.Dispose ();
				ExecuteUnitTestsButton = null;
			}

			if (LoadConfigFileButton != null) {
				LoadConfigFileButton.Dispose ();
				LoadConfigFileButton = null;
			}

			if (LoadedConfigFileReference != null) {
				LoadedConfigFileReference.Dispose ();
				LoadedConfigFileReference = null;
			}

			if (RepeatUnitTestsCheckboxReference != null) {
				RepeatUnitTestsCheckboxReference.Dispose ();
				RepeatUnitTestsCheckboxReference = null;
			}

			if (ServerAddressTextField != null) {
				ServerAddressTextField.Dispose ();
				ServerAddressTextField = null;
			}

			if (ServerPortTextField != null) {
				ServerPortTextField.Dispose ();
				ServerPortTextField = null;
			}

			if (UnitTestDirectoryLabelTextField != null) {
				UnitTestDirectoryLabelTextField.Dispose ();
				UnitTestDirectoryLabelTextField = null;
			}

			if (UnitTestDirectoryTextField != null) {
				UnitTestDirectoryTextField.Dispose ();
				UnitTestDirectoryTextField = null;
			}

			if (ConfigParametersTextFieldReference != null) {
				ConfigParametersTextFieldReference.Dispose ();
				ConfigParametersTextFieldReference = null;
			}
		}
	}

	[Register ("MainWindow")]
	partial class MainWindow
	{
		
		void ReleaseDesignerOutlets ()
		{
		}
	}
}
