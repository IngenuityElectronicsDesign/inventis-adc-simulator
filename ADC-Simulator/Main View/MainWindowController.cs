﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using MonoMac.Foundation;
using MonoMac.AppKit;
using System.Xml.Serialization;
using System.Timers;
using Newtonsoft.Json;

namespace ADCSimulator
{
	public partial class MainWindowController : MonoMac.AppKit.NSWindowController
	{
		private ConfigFileBrowserDelegate m_ConfigFileBrowserDelegate;
		private int m_SelectedConfigIndex;
		private ADCDataArray m_ADCDataArray;

		private int m_SelectedADCTab = 0;
		private const int m_ADCInfoViewControllerArrayCount	= 10;
		private ADCInfoViewController[] m_ADCInfoViewControllerArray;

		// Unit Testing data
		private string[] m_UnitTestFiles = null;
		private int m_CurrentUnitTestFileIndex = 0;
		private UnitTestStateMachineEnum m_UnitTestState = UnitTestStateMachineEnum.Idle;
		private List<int> m_UnitTestExecuting = new List<int>();
		private int m_UnitTestDelayTime = 0;						// a small delay as default
		private Timer m_UnitTestTimer = new Timer ();

		#region Constructors

		// Called when created from unmanaged code
		public MainWindowController (IntPtr handle) : base (handle)
		{
			Initialize ();
		}
		// Called when created directly from a XIB file
		[Export ("initWithCoder:")]
		public MainWindowController (NSCoder coder) : base (coder)
		{
			Initialize ();
		}
		// Call to load from the XIB/NIB file
		public MainWindowController () : base ("MainWindow")
		{
			Initialize ();
		}
		// Shared initialization code
		void Initialize ()
		{
			AppConfigController initialiseInstance = AppConfigController.Instance;
			m_UnitTestTimer.Elapsed += OnUnitTestTimerElasped;
		}

		#endregion

		//strongly typed window accessor
		public new MainWindow Window {
			get {
				return (MainWindow)base.Window;
			}
		}

		private void Debug(string debugString) {
			string debugOutputString = "----" + debugString;
			Console.WriteLine (debugOutputString);
			ActivityDebugCallback (debugOutputString);
		}

		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();

			m_ADCInfoViewControllerArray = new ADCInfoViewController[10];

			// Buttons
			LoadConfigFileButton.Activated += LoadConfigFileButtonActivatedHandler;
			ExecuteUnitTestsButton.Activated += ExecuteUnitTestsButtonActivatedHandler;
			RepeatUnitTestsCheckboxReference.Activated += RepeatUnitTestsButonActivatedHandler;

			// Directory Locations
			ConfigFileDirectoryTextField.EditingEnded += ConfigFileDirectoryTextFieldEditingEndedHandler;
			UnitTestDirectoryTextField.EditingEnded += UnitTestDirectoryTextFieldEditingEndedHandler;

			// Server Address and Port
			ServerAddressTextField.EditingEnded += ServerAddressTextFieldEditingEndedHandler;
			ServerPortTextField.EditingEnded += ServerPortTextFieldEditingEndedHandler;

			ServerAddressTextField.StringValue = AppConfigController.Instance.m_AppConfig.ServerAddress;
			ServerPortTextField.StringValue = AppConfigController.Instance.m_AppConfig.ServerPort.ToString();


			ConfigFileBrowser.SetTitle ("Select Config File", 0);
			ConfigFileBrowser.Activated += FileBrowserActivatedHandler;
			ConfigFileBrowser.Delegate = m_ConfigFileBrowserDelegate = new ConfigFileBrowserDelegate ();

			// m_ConfigFileBrowserDelegate holds the directory stored in the AppConfig xml
			ConfigFileDirectoryTextField.StringValue = AppConfigController.Instance.m_AppConfig.ConfigFileDirectory;
			LoadConfigFileDirectory (ConfigFileDirectoryTextField.StringValue);

			UnitTestDirectoryTextField.StringValue = AppConfigController.Instance.m_AppConfig.UnitTestDirectory;
			SetUnitTestDirectory (UnitTestDirectoryTextField.StringValue);

			RepeatUnitTestsCheckboxReference.State = AppConfigController.Instance.m_AppConfig.RepeatUnitTests ? NSCellStateValue.On : NSCellStateValue.Off;

			// Properties for the Activity Debug Text View
			System.Drawing.SizeF bigSize = new System.Drawing.SizeF (1000000, 1000000);
			ActivityTextViewReference.MaxSize = bigSize;
			ActivityTextViewReference.EnclosingScrollView.HasHorizontalScroller = true;
			ActivityTextViewReference.HorizontallyResizable = true;
			ActivityTextViewReference.AutoresizingMask = NSViewResizingMask.WidthSizable | NSViewResizingMask.HeightSizable;
			ActivityTextViewReference.TextContainer.ContainerSize = bigSize;
			ActivityTextViewReference.TextContainer.WidthTracksTextView = false;
			NSFont font = NSFont.FromFontName("Courier",12.0f);
			ActivityTextViewReference.Font = font;


			ADCTabViewReference.DidSelect += ADCTabDidSelectHandler;

			m_SelectedConfigIndex = -1;

		}

		private void ActivityDebugCallback( string debugString ) {
			InvokeOnMainThread (() => {
				ActivityTextViewReference.Value += debugString;
				ActivityTextViewReference.Value += "\n";
				ActivityTextViewReference.ScrollRangeToVisible( new NSRange(ActivityTextViewReference.Value.Length,0) );
			});
		}

		private void LoadConfigFileDirectory( string configFileDirectory ) {
			if ( m_ConfigFileBrowserDelegate.SetConfigFileDirectory( configFileDirectory ) ) {
				// this directory exists - tell theuser it's ok
				ConfigFileDirectoryLabelTextField.StringValue = "Config File Directory OK";
			} else {
				ConfigFileDirectoryLabelTextField.StringValue = "Config File Directory Does Not Exist";
			}
			ConfigFileBrowser.ReloadColumn (0);
		}

		private void ConfigFileDirectoryTextFieldEditingEndedHandler( object sender, EventArgs e) {
			LoadConfigFileDirectory (ConfigFileDirectoryTextField.StringValue);
		}

		private void SetUnitTestDirectory( string unitTestDirectory ) {
			if (Directory.Exists (unitTestDirectory)) {
				// this directory exists - set it
				AppConfigController.Instance.m_AppConfig.UnitTestDirectory = unitTestDirectory;

				UnitTestDirectoryLabelTextField.StringValue = "Unit Test Directory OK";

				// clear out old files
				m_UnitTestFiles = null;
				// add new files
				m_UnitTestFiles = Directory.GetFiles (AppConfigController.Instance.m_AppConfig.UnitTestDirectory, "*.xml");
				Console.WriteLine ("Directory Exists - FileCount:" + m_UnitTestFiles.Length);

				// save it so we remember!
				AppConfigController.Instance.SaveAppConfig ();
			} else {
				// otherwise clear everything
				Console.WriteLine ("Directory Does Not Exist");
				AppConfigController.Instance.m_AppConfig.UnitTestDirectory = "";
				UnitTestDirectoryLabelTextField.StringValue = "Unit Test Directory Does Not Exist";
				m_UnitTestFiles = null;
			}
		}

		private void UnitTestDirectoryTextFieldEditingEndedHandler( object sender, EventArgs e) {
			SetUnitTestDirectory (UnitTestDirectoryTextField.StringValue);
		}

		private void ServerAddressTextFieldEditingEndedHandler( object sender, EventArgs e) {
			AppConfigController.Instance.m_AppConfig.ServerAddress = ServerAddressTextField.StringValue;
			AppConfigController.Instance.SaveAppConfig ();
		}

		private void ServerPortTextFieldEditingEndedHandler( object sender, EventArgs e) {
			AppConfigController.Instance.m_AppConfig.ServerPort = ServerPortTextField.StringValue;
			AppConfigController.Instance.SaveAppConfig ();
		}

		private void LoadConfigFileButtonActivatedHandler( object sender, EventArgs e) {
			string configFileToLoad = m_ConfigFileBrowserDelegate.ConfigFileName (m_SelectedConfigIndex);
			Console.WriteLine("LoadConfigFileButtonActivatedHandler: Load row:" + m_SelectedConfigIndex + " File:" + configFileToLoad );
			LoadADCData (configFileToLoad);
		}

		private void SetExecuteUniteTestButtonState( bool on ) {
			if (on) {
				ExecuteUnitTestsButton.Title = "Executing Unit Tests";
				ExecuteUnitTestsButton.State = NSCellStateValue.On;
			} else {
				ExecuteUnitTestsButton.Title = "Execute Unit Tests";
				ExecuteUnitTestsButton.State = NSCellStateValue.Off;
			}
		}

		private void ExecuteUnitTestsButtonActivatedHandler( object sender, EventArgs e) {
			Console.WriteLine("ExecuteUnitTestsButtonActivatedHandler");
			if (m_UnitTestFiles == null) {
				Debug ("Unit Tests - No Files in Unit Test Directory");
				SetExecuteUniteTestButtonState (false);
				return;
			}
			// this is the state of the button AFTER we have just clicked it
			// So if we have clicked it and it's now off, we want to stop the testing
			if (ExecuteUnitTestsButton.State == NSCellStateValue.Off) {
				TerminateUnitTests ();
			} else {
				UnitTestNextState (true);
			}
		}

		private void RepeatUnitTestsButonActivatedHandler( object sender, EventArgs e) {
			AppConfigController.Instance.m_AppConfig.RepeatUnitTests = !(RepeatUnitTestsCheckboxReference.State == NSCellStateValue.Off);
			Console.WriteLine ("Unit Tests Repeat:" + AppConfigController.Instance.m_AppConfig.RepeatUnitTests);
			AppConfigController.Instance.SaveAppConfig ();
		}

		private enum UnitTestStateMachineEnum {
			Idle,
			LoadFile,
			ExecutingTest,
			WaitingForDelayCompletion,

			Count
		}

		private void UnitTestNextState(bool start) {
			if (start) {
				m_CurrentUnitTestFileIndex = 0;
				m_UnitTestState = UnitTestStateMachineEnum.LoadFile;
			}

			switch (m_UnitTestState) {
				case UnitTestStateMachineEnum.Idle:
					// not sure what we should be doing here!
					break;
				case UnitTestStateMachineEnum.LoadFile:
					if (m_UnitTestFiles[m_CurrentUnitTestFileIndex] == null) {
						Debug ("Unit Tests - StateMachine - file: null!");
						m_UnitTestState = UnitTestStateMachineEnum.Idle;
						SetExecuteUniteTestButtonState (false);
						return;
					}

					Debug ("Unit Tests - Load File:" + m_UnitTestFiles[m_CurrentUnitTestFileIndex]);
					LoadADCData (m_UnitTestFiles[m_CurrentUnitTestFileIndex]);

					m_UnitTestState = UnitTestStateMachineEnum.ExecutingTest;
					Debug ("Unit Tests - Execute");
					foreach (ADCInfoViewController adcInfoView in m_ADCInfoViewControllerArray) {
						if (adcInfoView != null) {
							Console.WriteLine ("     ADC:" + adcInfoView.ADCName ());
							m_UnitTestExecuting.Add(adcInfoView.ADCId());
							adcInfoView.InitiateComms (UnitTestCompleted);
						}
					}
					break;
				case UnitTestStateMachineEnum.ExecutingTest:
					if (m_CurrentUnitTestFileIndex < (m_UnitTestFiles.Count()-1)) {
						// keep going through the tests
						m_CurrentUnitTestFileIndex++;
						Debug ("Unit Tests - Next File: " + m_CurrentUnitTestFileIndex);
					} else {
						// we are at the end
						if (AppConfigController.Instance.m_AppConfig.RepeatUnitTests) {
							// start again
							Debug ("Unit Tests - Next File: Back to Start");
							m_CurrentUnitTestFileIndex = 0;
						} else {
							// done
							Debug ("Unit Tests - Done (ExecutingTest-next file end)");
							m_UnitTestState = UnitTestStateMachineEnum.Idle;
							SetExecuteUniteTestButtonState (false);
							break;
						}
					}
					if (m_UnitTestFiles [m_CurrentUnitTestFileIndex] != null) {
						// there is a new file, lets load it after the specified delay time
						m_UnitTestState = UnitTestStateMachineEnum.WaitingForDelayCompletion;
						m_UnitTestTimer.Interval = m_UnitTestDelayTime <= 0 ? 1 : m_UnitTestDelayTime ;		// we can't make a negative or zero length timer - just make it short
						m_UnitTestTimer.AutoReset = false;
						m_UnitTestTimer.Start ();
						Debug ("Unit Tests - Timer started");
					} else {
						m_UnitTestState = UnitTestStateMachineEnum.Idle;
						Debug ("Unit Tests - Done (ExecutingTest)");
						SetExecuteUniteTestButtonState (false);
					}
					break;
				case UnitTestStateMachineEnum.WaitingForDelayCompletion:
					if (m_CurrentUnitTestFileIndex < m_UnitTestFiles.Count()) {
						if (m_UnitTestFiles [m_CurrentUnitTestFileIndex] != null) {
							m_UnitTestState = UnitTestStateMachineEnum.LoadFile;
							// find the next file
							Debug ("Unit Tests - Next File:" + m_CurrentUnitTestFileIndex );
							UnitTestNextState (false);
						} else {
							// invalid file
							m_UnitTestState = UnitTestStateMachineEnum.Idle;
							Debug ("Unit Tests - Done (WaitingForDelayCompletion-invalid file)");
							SetExecuteUniteTestButtonState (false);
						}
					} else {
						// off the end of hte list
						m_UnitTestState = UnitTestStateMachineEnum.Idle;
						Debug ("Unit Tests - Done (WaitingForDelayCompletion)");
						SetExecuteUniteTestButtonState (false);
					}
					break;

			}
		}

		private void TerminateUnitTests() {
			Debug ("Unit Tests - Terminated");
			m_UnitTestTimer.Stop ();
			m_UnitTestExecuting.Clear ();
			InvokeOnMainThread (() => {
				m_UnitTestState = UnitTestStateMachineEnum.Idle;
				SetExecuteUniteTestButtonState (false);
			});
		}

		private void OnUnitTestTimerElasped (object o, EventArgs e)
		{
			if (m_UnitTestState != UnitTestStateMachineEnum.WaitingForDelayCompletion) {
				Debug ("OnUnitTestTimerElasped - called in bad state:" + m_UnitTestState );
			} else {
				m_UnitTestTimer.Stop ();
				Debug ("Unit Tests - Timer Completed");
				InvokeOnMainThread (() => {
					UnitTestNextState (false);
				});
			}
		}

		private void UnitTestCompleted(int adcId) {
			Debug ("Unit Tests - Unit Test Completed:" + adcId + " -- Count:" + m_UnitTestExecuting.Count );
			m_UnitTestExecuting.Remove (adcId);
			InvokeOnMainThread (() => {
				if (m_UnitTestExecuting.Count == 0) {
					Debug ("Unit Tests - All Unit Tests Completed - Next State");
					UnitTestNextState (false);
				}
			});
		}

		private void FileBrowserActivatedHandler( object sender, EventArgs e) {
			m_SelectedConfigIndex = ((NSBrowser)sender).SelectedRow (0);
			Console.WriteLine("FileBrowserActivatedHandler:" + ((NSBrowser)sender).SelectedRow(0) );
		}

		private void ADCTabDidSelectHandler( object sender, EventArgs e) {
			NSTabView selectedTabView = (NSTabView)sender;
			NSTabViewItem selectedTabViewItem = selectedTabView.Selected;
			m_SelectedADCTab = selectedTabView.IndexOf (selectedTabViewItem);
			Console.WriteLine("ADCTabDidSelectHandler:" + m_SelectedADCTab );
		}

		private bool LoadADCData( string adcDataFilename ) {

			LoadedConfigFileReference.StringValue = "Config File: " + Path.GetFileName(adcDataFilename);

			int tabCount = ADCTabViewReference.Items.Length;

			for (int i=0; i<tabCount; i++ ) {
				// remove tab
				Console.WriteLine ("Remove tab:" + i);
				ADCTabViewReference.Remove(ADCTabViewReference.Item(0));
			}

			try {
				using (TextReader reader = new StreamReader(adcDataFilename)) {
					XmlSerializer serializer = new XmlSerializer(typeof(ADCDataArray));
					m_ADCDataArray = (ADCDataArray)serializer.Deserialize(reader);

					/*
					TextWriter writer = new StreamWriter( Path.Combine( Path.GetDirectoryName(adcDataFilename), "Test.xml" ) );
					serializer.Serialize(writer,m_ADCDataArray);
					writer.Close();
					*/

					// Display the parameters for this config
					ConfigParametersTextFieldReference.StringValue = "Config Parameters\n" + 
						"Unit Test Delay: " + m_ADCDataArray.UnitTestDelay.ToString() + "\n" +
						"Set Created To Now: " + m_ADCDataArray.SetCreatedToNow.ToString();

					m_UnitTestDelayTime = m_ADCDataArray.UnitTestDelay;
					Debug("Unit Tests - Delay Time:" + m_UnitTestDelayTime );

					Console.WriteLine ("Found " + m_ADCDataArray.ADCDataArrayObject.Length + " ADCDataArray Objects");

					int adcCounter = 0;

					foreach ( ADCHeader adcData in m_ADCDataArray.ADCDataArrayObject ) {

						// remove the old view controller from memory
						m_ADCInfoViewControllerArray[adcCounter] = null;

						// create the new veiw controller for this tab
						m_ADCInfoViewControllerArray[adcCounter] = new ADCInfoViewController(adcCounter,adcData,m_ADCDataArray.SetCreatedToNow,ActivityDebugCallback);

						NSTabViewItem adcViewItem = new NSTabViewItem();

						adcViewItem.View = m_ADCInfoViewControllerArray[adcCounter].View;
						adcViewItem.Label = "ADC " + (adcCounter+1).ToString();

						ADCTabViewReference.Add(adcViewItem);

						//m_ADCInfoViewControllerArray[adcCounter].InitiateCommsButton.Activated += ADCInitiateCommsButtonActivatedHandler;

						NSFont font = NSFont.FromFontName("Courier",14.0f);

						NSTextView thisTextView = m_ADCInfoViewControllerArray[adcCounter].ADCInfoTextView;

						thisTextView.Font = font;

						thisTextView.Value = JsonConvert.SerializeObject(adcData,Formatting.Indented);

						adcCounter++;
					}
				}
			}
			catch ( Exception e ) {
				if (e != null) {
					Console.WriteLine ("LoadADCData - Exception Caught:" + e.Message);
					if (e.InnerException != null) {
						Console.WriteLine ("                                :" + e.InnerException.Message);
					}
				} else {
					Console.WriteLine ("LoadADCData - Exception Caught:null");
				}
				m_ADCDataArray = null;
				return false;
			}

			return true;
		}

	}
}

