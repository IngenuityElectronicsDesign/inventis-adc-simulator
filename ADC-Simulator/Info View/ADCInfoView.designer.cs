// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoMac.Foundation;
using System.CodeDom.Compiler;

namespace ADCSimulator
{
	[Register ("ADCInfoViewController")]
	partial class ADCInfoViewController
	{
		[Outlet]
		MonoMac.AppKit.NSTextView ADCInfoTextViewReference { get; set; }

		[Outlet]
		MonoMac.AppKit.NSButton InitiateCommsButtonReference { get; set; }

		[Outlet]
		MonoMac.AppKit.NSTextField LastError1TextFieldReference { get; set; }

		[Outlet]
		MonoMac.AppKit.NSTextField LastError2TextFieldReference { get; set; }

		[Outlet]
		MonoMac.AppKit.NSTextField StatusTextFieldReference { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (ADCInfoTextViewReference != null) {
				ADCInfoTextViewReference.Dispose ();
				ADCInfoTextViewReference = null;
			}

			if (InitiateCommsButtonReference != null) {
				InitiateCommsButtonReference.Dispose ();
				InitiateCommsButtonReference = null;
			}

			if (StatusTextFieldReference != null) {
				StatusTextFieldReference.Dispose ();
				StatusTextFieldReference = null;
			}

			if (LastError1TextFieldReference != null) {
				LastError1TextFieldReference.Dispose ();
				LastError1TextFieldReference = null;
			}

			if (LastError2TextFieldReference != null) {
				LastError2TextFieldReference.Dispose ();
				LastError2TextFieldReference = null;
			}
		}
	}

	[Register ("ADCInfoView")]
	partial class ADCInfoView
	{
		
		void ReleaseDesignerOutlets ()
		{
		}
	}
}
