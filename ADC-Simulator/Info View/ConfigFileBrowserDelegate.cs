﻿using System;
using System.IO;
using MonoMac.AppKit;
using MonoMac.Foundation;
using System.Xml.Serialization;

namespace ADCSimulator
{
	public class ConfigFileBrowserDelegate : NSBrowserDelegate
	{
		private string[]	m_ConfigFiles;

		public ConfigFileBrowserDelegate ()
		{
		}

		public override int RowsInColumn (NSBrowser sender, int column)
		{
			if ( m_ConfigFiles == null ) return 0;
			return m_ConfigFiles.Length;
		}

		public override void WillDisplayCell (NSBrowser sender, MonoMac.Foundation.NSObject cell, int row, int column)
		{
			((NSBrowserCell)cell).Leaf = true;
			if ( m_ConfigFiles == null ) ((NSBrowserCell)cell).Title = "";
			if (row >= 0 && row < m_ConfigFiles.Length) {
				((NSBrowserCell)cell).Title = Path.GetFileName(m_ConfigFiles [row]);
			} else {
				Console.Error.WriteLine ("ConfigFileBrowserDelegate - WillDisplayCell - row index out of range:" + row + " > " + m_ConfigFiles.Length );
			}
		}

		public string ConfigFileName( int row ) {
			if ( m_ConfigFiles == null ) return "";
			if (row >= 0 && row < m_ConfigFiles.Length) {
				return m_ConfigFiles [row];
			} else {
				Console.Error.WriteLine ("ConfigFileBrowserDelegate - ConfigFileName - row index out of range:" + row + " > " + m_ConfigFiles.Length );
				return "";
			}
		}

		public bool SetConfigFileDirectory( string configFileDirectory ) {
			if (Directory.Exists (configFileDirectory)) {
				// this directory exists - set it
				AppConfigController.Instance.m_AppConfig.ConfigFileDirectory = configFileDirectory;

				// clear out old files
				m_ConfigFiles = null;
				// add new files
				m_ConfigFiles = Directory.GetFiles (AppConfigController.Instance.m_AppConfig.ConfigFileDirectory, "*.xml");
				Console.WriteLine ("Directory Exists - FileCount:" + m_ConfigFiles.Length);

				// save it so we remember!
				AppConfigController.Instance.SaveAppConfig ();

				return true;
			} else {
				// otherwise clear everything
				Console.WriteLine ("Directory Does Not Exist");
				AppConfigController.Instance.m_AppConfig.ConfigFileDirectory = "";
				m_ConfigFiles = null;
				return false;
			}
		}

	}
}

