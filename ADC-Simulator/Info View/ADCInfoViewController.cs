﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using MonoMac.Foundation;
using MonoMac.AppKit;

namespace ADCSimulator
{
	public partial class ADCInfoViewController : MonoMac.AppKit.NSViewController
	{
		private int m_id;
		private ADCHeader m_adcData;
		private bool m_setCreatedToNow;
		private AsynchronousClient m_asyncClient;
		private Action<int> m_TerminatedCommsCallback;
		private Action<string> m_ActivityDebugCallback;

		#region Constructors

		// Called when created from unmanaged code
		public ADCInfoViewController (IntPtr handle) : base (handle)
		{
			Initialize ();
		}
		// Called when created directly from a XIB file
		[Export ("initWithCoder:")]
		public ADCInfoViewController (NSCoder coder) : base (coder)
		{
			Initialize ();
		}
		// Call to load from the XIB/NIB file
		public ADCInfoViewController (int id, ADCHeader adcData, bool setCreatedToNow, Action<string> activityDebugCallback) : base ("ADCInfoView", NSBundle.MainBundle)
		{
			m_id = id;
			m_adcData = adcData;
			m_setCreatedToNow = setCreatedToNow;
			m_asyncClient = new AsynchronousClient();
			m_ActivityDebugCallback = activityDebugCallback;
			Initialize ();
		}
		// Shared initialization code
		void Initialize ()
		{
		}

		#endregion

		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();
			InitiateCommsButtonReference.Activated += ADCInitiateCommsButtonActivatedHandler;
		}

		//strongly typed view accessor
		public new ADCInfoView View {
			get {
				return (ADCInfoView)base.View;
			}
		}

		public NSTextView ADCInfoTextView {
			get {
				return ADCInfoTextViewReference;
			}
		}

		public string ADCName() {
			return m_adcData.Site + "_" + m_adcData.Dest1 + "_" + m_adcData.Dest2;
		}

		public int ADCId() {
			return m_id;
		}

		private void Debug(string debugString) {
			string debugOutputString = "[" + m_id + "] === " + debugString;
			Console.WriteLine (debugOutputString);
			if (m_ActivityDebugCallback != null) {
				m_ActivityDebugCallback (debugOutputString);
			}
		}

		private void DebugError(string debugString) {
			string debugOutputString = "[" + m_id + "] === " + debugString;
			Console.Error.WriteLine (debugOutputString);
			if (m_ActivityDebugCallback != null) {
				m_ActivityDebugCallback (debugOutputString);
			}
		}

		private void ADCInitiateCommsButtonActivatedHandler( object sender, EventArgs e) {
			NSButton button = (NSButton)sender;
			Debug("ADCInitiateCommsButtonActivatedHandler for ADC");
			if (button.State == NSCellStateValue.On) {
				InitiateComms (null); // we don't need a callback - we are here!
			} else {
				TerminateComms ();
			}
		}

		private void SetButtonState( bool on ) {
			if (on) {
				InitiateCommsButtonReference.Title = "Terminate Comms";
				InitiateCommsButtonReference.State = NSCellStateValue.On;
			} else {
				InitiateCommsButtonReference.Title = "Initiate Comms";
				InitiateCommsButtonReference.State = NSCellStateValue.Off;
			}
		}

		public bool InitiateComms( Action<int> terminateCommsCallback ) {

			m_TerminatedCommsCallback = terminateCommsCallback;

			Debug("Initiate Comms");

			SetButtonState (true);

			if (m_asyncClient == null) {
				DebugError("InitiateComms - null TCP client");
				if (m_TerminatedCommsCallback != null) {
					m_TerminatedCommsCallback (m_id);
				}
				return false;
			}

			// Set the Created DateTimes in each message packet to reflect the current time
			if ( m_setCreatedToNow ) {
				Debug ("Set ADC Created to: " + DateTime.UtcNow);
				m_adcData.Created =  ADC_IRS_Comms.ConvertToUnixTimestamp(DateTime.Now);
				if (m_adcData.RouterLogs != null) {
					foreach (ADCRouterLog log in m_adcData.RouterLogs) {
						log.Created = m_adcData.Created;
					}
				}
				if (m_adcData.EventLogs != null) {
					foreach (ADCEventLog elog in m_adcData.EventLogs) {
						elog.Created = m_adcData.Created;
					}
				}
			}

			//Uses the IP address and port number to establish a socket connection.
			m_asyncClient.StartClient ( AsyncClientStatusCallback, m_id, m_adcData, AppConfigController.Instance.m_AppConfig.ServerAddress,AppConfigController.Instance.m_AppConfig.ServerPort, m_ActivityDebugCallback );

			return true;
		}

		private void AsyncClientStatusCallback( AsynchronousClient.StateMachineEnum asyncCommsState, Exception e ) {
			InvokeOnMainThread (() => {
				StatusTextFieldReference.StringValue = asyncCommsState.ToString ();
				if ( asyncCommsState == AsynchronousClient.StateMachineEnum.ConnectionClosed ) {
					SetButtonState (false);
					// We are done - tell whoever set us up we are terminated
					if (m_TerminatedCommsCallback != null) {
						m_TerminatedCommsCallback (m_id);
					}
				} else if ( asyncCommsState >= AsynchronousClient.StateMachineEnum.Count ) {
					// We are in an invalid state - tell whoever set us up we are terminated
					if (m_TerminatedCommsCallback != null) {
						m_TerminatedCommsCallback (m_id);
					}
					SetButtonState (false);
				} else {
					SetButtonState (true);
				}
				if (e != null) {
					Debug("Exception: " + e.Message);
					LastError1TextFieldReference.StringValue = e.Message;
					if (e.InnerException != null) {
						Debug("         :" + e.InnerException.Message);
						LastError2TextFieldReference.StringValue = e.InnerException.Message;
					} else {
						LastError2TextFieldReference.StringValue = "";
					}
				} else {
					LastError1TextFieldReference.StringValue = "";
					LastError2TextFieldReference.StringValue = "";
				}
			});
		}

		public void TerminateComms() {
			Debug("Terminate Comms");
			SetButtonState (false);
			m_asyncClient.StopClient ();
			// and we are done
			if (m_TerminatedCommsCallback != null) {
				m_TerminatedCommsCallback (m_id);
			}
		}

	}
}

